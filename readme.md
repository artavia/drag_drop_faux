# Brief description
A touch API&#45;friendly faux drag drop exhibit&#45;&#45; for edge&#45;cases requiring interactive results via touch &#45;&#45;which substitutes for actual drag drop as text in terms of javascript mechanics.

## Please visit
You can see [the project](https://artavia.gitlab.io/drag_drop_faux "the project at github") for yourself and decide whether it is really for you or not. 

## A lesson to those with loose lips
This exhibit is fluff. It&rsquo;s only purpose is to make you smile. It does nothing other than break expectations. When somebody says that something cannot be done it only makes me curious to know the motivation behind such a careless statement; in this case, somebody previously stated that drag and drop cannot be executed on mobile devices and I beg to differ with all due respect.

### Kudos
Hat-tip to all of you generative and kind developers out there. This is a refax of [one of several sources](http://popdevelop.com/2010/08/touching-the-web "link to an external example"), although, presently I am really unsure [of who actually helped me the most](http://sewa.se/drag/ "link to faux drag and drop lesson") at that specific time. It makes no difference. I thank all of you. 