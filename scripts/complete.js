var DRYOBJ = ( function ( ) {
	
	/*################  ES5 pragma  ######################*/
	'use strict';	
	
	function _RetValFalprevDef( leEvt ) { 
		if( typeof leEvt !== "undefined") {
			var _EREF = leEvt; 
		} 
		else {
			var _EREF = window.event; 
		}		
		if( _EREF.preventDefault) {
			_EREF.preventDefault(); 
		}
		else {
			_EREF.returnValue = false; 
		}
	}

	function _RetEVTsrcEL_evtTarget( leEvt ) { 
		if( typeof leEvt !== "undefined") { 
			var _EREF = leEvt; 
		}
		else {
			var _EREF = window.event; 
		}
		if( typeof _EREF.target !== "undefined") {
			var evtTrgt = _EREF.target; 
		}
		else {
			var evtTrgt = _EREF.srcElement; 
		}
		return evtTrgt;
	}
	
	function _AddEventoHandler( nodeFlanders, type, callback ) {
		if( type !== "DOMContentLoaded") { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}		
			else	
			if( nodeFlanders.attachEvent ) { 
				nodeFlanders.attachEvent( "on" + type, callback );
			} 		
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
		else 
		if( type === "DOMContentLoaded" ) { 
			if( nodeFlanders.addEventListener ) { 
				nodeFlanders.addEventListener( type, callback, false);
			}
			else 
			if( nodeFlanders.attachEvent ) { 
				if( nodeFlanders.readyState === "loading" ) {
					nodeFlanders.onreadystatechange = callback;
				}
			}
			else { 
				nodeFlanders["on" + type] = callback; 
			}
		}
	}

	return {
		Utils : {
			evt_u : {
				AddEventoHandler : function( nodeFlanders, type, callback ) {
					return _AddEventoHandler( nodeFlanders, type, callback );
				} , 
				RetEVTsrcEL_evtTarget : function( leEvt ) {
					return _RetEVTsrcEL_evtTarget( leEvt );
				}	, 
				RetValFalprevDef : function( leEvt ) { 
					return _RetValFalprevDef( leEvt );
				} 
			} 
		}
			
	}; // END public properties
	
}( ) ); // console.log( DRYOBJ ); 


var Gh_pages = ( function() {

	/*################  ES5 pragma  ######################*/
	'use strict';
	var _docObj = window.document;	
	var _offset_DDfo = false;				
	
	var _ar_fauxDraggables = [];
	var _Z;
	var _ar_fauxDZMax;	
	
	var _el_fauxMemb = _docObj.getElementById( "fauxMemb" );	
	
	var _allDraggableDivs = _docObj.getElementsByTagName( "div" );
	var _Y;	
	
	var _ar_CHGDtouches;
	var _fingerMoveCount;	
	
	var _fingerCount;
	var _W;	
	
	var _startX; 
	var _startY; 
	var _lastX; 
	var _lastY; 

	function _PrevDeffo(leEvt) {
		DRYOBJ.Utils.evt_u.RetValFalprevDef(leEvt);
	} // END function
	
	function _TS_Dd_faux( leEvt ) { 	
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerCount = _ar_CHGDtouches.length;

		_W = 0;
		
		if ( _fingerCount === 1 && !_offset_DDfo ) {		
			for( _W; _W < _fingerCount; _W = _W + 1 ) { 
			
				if ( evtTrgt.tagName.toLowerCase() === "div" ) { 
					
					_startX = _ar_CHGDtouches[ _W ].pageX; 
					
					_startY = _ar_CHGDtouches[ _W ].pageY; 
					
					// console.log( "START _startX" , _startX );
					// console.log( "START _startY" , _startY );
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					// console.log("touchTgtZero" , touchTgtZero );	
					
					var GetOffsetLeft = touchTgtZero.offsetLeft;
					// console.log( "GetOffsetLeft" , GetOffsetLeft );
					
					var GetOffsetTop = touchTgtZero.offsetTop;
					// console.log( "GetOffsetTop" , GetOffsetTop );
					
					var ComputeMargTop = document.defaultView.getComputedStyle( touchTgtZero, "" ).getPropertyValue( "margin-top" ).slice( 0 , document.defaultView.getComputedStyle( touchTgtZero, "" ).getPropertyValue( "margin-top" ).length - 2 );
					// console.log("ComputeMargTop" , ComputeMargTop);
					
					var ComputeMargLeft = document.defaultView.getComputedStyle( touchTgtZero, "" ).getPropertyValue( "margin-left" ).slice( 0 , document.defaultView.getComputedStyle( touchTgtZero, "" ).getPropertyValue( "margin-left" ).length - 2 );
					
					// console.log("ComputeMargLeft", ComputeMargLeft );
					
					var TopQty = GetOffsetTop - ComputeMargTop; 
					// console.log("TopQty" , TopQty );
					
					var LeftQty = GetOffsetLeft - ComputeMargLeft; 
					// console.log("LeftQty" , LeftQty);
					
					_offset_DDfo = {
						x : ( _startX ) - GetOffsetLeft , 
						y : ( _startY ) - GetOffsetTop
					}; 
					
					// console.log( "_offset_DDfo.x" , _offset_DDfo.x );
					// console.log( "_offset_DDfo.y" , _offset_DDfo.y );
				
				}
			}
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	
	function _TM_Dd_faux( leEvt ) {
		
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerMoveCount = _ar_CHGDtouches.length;
		_W = 0;
		
		if ( _fingerMoveCount === 1 ) { 
			for( _W; _W < _fingerMoveCount; _W = _W + 1 ) { 
				if ( _fingerMoveCount === 1 && evtTrgt.tagName.toLowerCase() === "div" && !!_offset_DDfo ) { 
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					// console.log( "touchTgtZero" ,  touchTgtZero );
					
					touchTgtZero.style.cssText = "left: " + ( (_ar_CHGDtouches[ _W ].pageX) - _offset_DDfo.x ) + "px; "; 
					touchTgtZero.style.cssText += "top: " + ( (_ar_CHGDtouches[ _W ].pageY) - _offset_DDfo.y ) + "px; "; 
					
					// console.log( "MOVE _ar_CHGDtouches[ _W ].pageX" , _ar_CHGDtouches[ _W ].pageX );	
					// console.log( "MOVE _ar_CHGDtouches[ _W ].pageY" , _ar_CHGDtouches[ _W ].pageY );
					
				}
			}
		}
		
		if ( !_fingerMoveCount === 1) { 
			_offset_DDfo = false;
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	
	function _TE_Dd_faux( leEvt ) { 
		
		var evtTrgt = DRYOBJ.Utils.evt_u.RetEVTsrcEL_evtTarget( leEvt ); 
		_ar_CHGDtouches = leEvt.changedTouches; 
		_fingerCount = _ar_CHGDtouches.length;
		_W = 0;
		
		if ( _fingerCount === 1 ) { 
			for( _W; _W < _fingerCount; _W = _W + 1 ) { 
				
				if ( _fingerCount === 1 && evtTrgt.tagName.toLowerCase() === "div" && !!_offset_DDfo ) { 
				
					_lastX = _ar_CHGDtouches[ _W ].pageX - _offset_DDfo.x; 
					_lastY = _ar_CHGDtouches[ _W ].pageY - _offset_DDfo.y; 
					// console.log("END _lastX" , _lastX );
					// console.log("END _lastY" , _lastY);
					
					var touchTgtZero = _ar_CHGDtouches[ _W ].target; 
					// console.log("touchTgtZero" , touchTgtZero);
					
					touchTgtZero.style.cssText = "left: " + ( _lastX ) + "px; "; 
					
					touchTgtZero.style.cssText += "top: " + ( _lastY ) + "px; "; 
					
					_offset_DDfo = false; 
				}
			}
		}
		if ( !_fingerCount === 1) {
			_offset_DDfo = false;
		}
		_PrevDeffo(leEvt); 
		
	} // END function
	
	function _DOMCONLO() {
		
		_Y = 0;
		_Z = 0;
		for (_Y; _Y < _allDraggableDivs.length; _Y = _Y + 1 ) {
			if( _allDraggableDivs[ _Y ].className === "faux_drag" ) { 
				_ar_fauxDraggables.push( _allDraggableDivs[ _Y ] );
				_ar_fauxDZMax = _ar_fauxDraggables.length;
				_ar_fauxDraggables.draggable = true; 
			}
		} 					
		for( _Z; _Z < _ar_fauxDZMax; _Z = _Z + 1 ){ 
			DRYOBJ.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchstart" , _TS_Dd_faux ); 
			DRYOBJ.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchmove" , _TM_Dd_faux ); 
			DRYOBJ.Utils.evt_u.AddEventoHandler( _ar_fauxDraggables[ _Z ], "touchend" , _TE_Dd_faux ); 
		}
		//
	}

	// END of _private properties
	return {
		InitDCL: function() {
			return _DOMCONLO(); 
		} 
	};

} )(); // window.Gh_pages

DRYOBJ.Utils.evt_u.AddEventoHandler( window , "DOMContentLoaded" , Gh_pages.InitDCL() );